# generated id for new project
output "project_id" {
  value = "${google_project.project.project_id}"
}

# generated number for new project
output "project_number" {
  value = "${google_project.project.number}"
}

# id for the project resource SA
output "terraform_admin_user_id" {
  value = "${google_service_account.terraform_admin.email}"
}

