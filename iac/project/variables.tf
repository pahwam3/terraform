variable "project_name" {
  description = "The human readable name for the project"
}
variable "project_folder_id" {
  description = "The id for the folder in which this project should be created"
}

variable "billing_account_id" {
  description = "The billing account id for the project"
}

variable "project_creator_key" {
  description = "The SA key for the project creator"
}

variable "project_apis" {
  description = "APIs to be enabled on the newly created project"
  type        = "list"

  default = [
    "iam.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "storage-api.googleapis.com",
    "oslogin.googleapis.com",
    "bigquery-json.googleapis.com",
    "containerregistry.googleapis.com",
    "pubsub.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
    "dataproc.googleapis.com",
    "storage-component.googleapis.com",
    "deploymentmanager.googleapis.com",
    "replicapool.googleapis.com",
    "replicapoolupdater.googleapis.com",
    "resourceviews.googleapis.com",
    "iamcredentials.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "stackdriver.googleapis.com",
  ]
}

variable "terraform_account_name" {
  description = "The name of the Terraform admin account to be created in the project"
  default     = "terraform-admin"
}


