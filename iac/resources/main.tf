# terraform main script
terraform {
  backend "gcs" {
     bucket = "tf-state-storage12345"
     prefix = "awsome/resources"
     credentials = "tf-state-key.json"
     project = "My Project terraform"
     region =  "europe_west2"
    }
}
# configure the Google Cloud provider
 provider "google" {
   version =  "1.18.0"
   project =  "${var.project_id}"
   region =  "${var.region}"
   zone = "${var.zone}"
   credentials =  "${var.terraform_admin_key}"
}

